﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using System.Collections;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class AudioWaveFormVisualizer : MonoBehaviour
{
    private int width; // texture width 
    public int height = 100; // texture height 
    public int size = 2048; // size of sound segment displayed in texture

    public Color backgroundColor = Color.black;
    public Gradient ColorGradient;
    private Color[] blank; // blank image array 
    private Texture2D textureLeft;
    private Texture2D textureRight;
    private float[] samplesLeft; // audio samples array
    private float[] samplesRight; // audio samples array

    private int step = 0;
    private AudioSource audioSource;

    public RawImage LeftSpectrumImage;
    public RawImage RightSpectrumImage;

    private bool play = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        // create the samples array 
        samplesLeft = new float[size];
        samplesRight = new float[size];
        // create the texture and assign to the guiTexture: 
        width = Screen.width;
        textureLeft = new Texture2D(width, height);
        textureRight = new Texture2D(width, height);
        LeftSpectrumImage.texture = textureLeft;
        RightSpectrumImage.texture = textureRight;

        // create a 'blank screen' image 
        blank = new Color[width * height];

        for (int i = 0; i < blank.Length; i++)
        {
            blank[i] = backgroundColor;
        }
        textureLeft.SetPixels(blank, 0);
        textureRight.SetPixels(blank, 0);

        // refresh the display each 100mS 
//        while (true)
//        {
//            GetCurWave();
//            yield return new WaitForSeconds(0.0001f);
//        }
        play = true;
    }

    void Update()
    {
        if (play)
        {
            GetCurWave();
        }
    }

    void GetCurWave()
    {
        ProcessSamples(samplesLeft, textureLeft, 0);
        ProcessSamples(samplesRight, textureRight, 1);
        
        step++;
    }

    void ProcessSamples(float[] channelSamples, Texture2D texture, int channel)
    {
        audioSource.GetSpectrumData(channelSamples, channel, FFTWindow.BlackmanHarris);

        for (int i = 0; i < size; i++)
        {
            texture.SetPixel(step, (int)(height * i / size),
                ColorGradient.Evaluate(channelSamples[i] * 100));
        }
        texture.Apply();
    }

    public static float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}